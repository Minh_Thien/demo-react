# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(
  [
    {
      first_name: "Nguyen",
      last_name: "Van A",
      email: "nguyenvana@gmail.com",
      password: "123456789"
    },
    {
      first_name: "Tran",
      last_name: "Van B",
      email: "tranvanb@gmail.com",
      password: "123456789"
    },
    {
      first_name: "Pham",
      last_name: "Thi C",
      email: "phamthic@gmail.com",
      password: "123456789"
    },
    {
      first_name: "Le",
      last_name: "Van D",
      email: "levand@gmail.com",
      password: "123456789"
    }
  ])