import React, { Component } from 'react';
import axios from 'axios';
class UsersList extends Component {
  constructor(props){
    super(props)
    this.state = {
        users: []
    }
  }

  componentDidMount() {
    axios.get('users.json').then(response => {
      this.setState({
          users: response.data
      })
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div className="users-container">
          {this.state.users.map( user => {
            return (
              <div className="user" key={user.id}>
                <h4>{user.first_name + ' ' + user.last_name}</h4>
                <p>{user.email}</p>
              </div>
            )
          })}
      </div>
    )
  }
}

export default UsersList;